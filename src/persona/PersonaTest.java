package persona;

import org.junit.Test;

public class PersonaTest 
{
	@Test(expected = IllegalArgumentException.class)
	public void nombreVacio()
	{
		Persona per = new Persona(" ",1,3,4,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesDeporteMenor1()
	{
		Persona per = new Persona("Julio",-1,3,4,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesDeporteMayor5()
	{
		Persona per = new Persona("Julio",7,3,4,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesMusicaMenor1()
	{
		Persona per = new Persona("Julio",1,-2,4,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesMusicaMayor5()
	{
		Persona per = new Persona("Julio",1,7,4,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesEspectaculoMenor1()
	{
		Persona per = new Persona("Julio",1,3,-1,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesEspectaculoMayor5()
	{
		Persona per = new Persona("Julio",2,3,8,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesCienciaMenor1()
	{
		Persona per = new Persona("Julio",1,3,4,-3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void interesCienciaMayor5()
	{
		Persona per = new Persona("Julio",3,3,4,9);
	}
}