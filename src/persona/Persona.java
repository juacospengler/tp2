package persona;

public class Persona 
{
	private String nombre;
	private int interesDeporte;
	private int interesMusica;
	private int interesEspectaculo;
	private int interesCiencia;
	
	public Persona(String nombre, int interesDeporte, int interesMusica, int interesEspectaculo, int interesCiencia) 
	{
		
		if (validarParamatros(nombre,  interesDeporte,  interesMusica,  interesEspectaculo,  interesCiencia))
		{
			this.nombre = nombre;
			this.interesDeporte = interesDeporte;
			this.interesMusica = interesMusica;
			this.interesEspectaculo = interesEspectaculo;
			this.interesCiencia = interesCiencia;
		} 
		else 
		{
			throw new IllegalArgumentException("Los parametros no son validos");
		}
	}

	private boolean validarParamatros(String nombre, int interesDeporte, int interesMusica, int interesEspectaculo,
			int interesCiencia) 
	{
		return nombre!= " " && estaEnRango(interesDeporte) &&  estaEnRango(interesMusica) && estaEnRango(interesEspectaculo) &&
				estaEnRango(interesCiencia);
	}

	public boolean iguales(Persona obj) 
	{
	    return  this.nombre             == obj.getNombre() &&
	    		this.interesDeporte     == obj.getInteresDeporte() &&
	    		this.interesMusica      == obj.getInteresMusica() &&
	    		this.interesEspectaculo == obj.getInteresEspectaculo() &&
	    		this.interesCiencia     == obj.getInteresCiencia();
	}

	private boolean estaEnRango(int parametro) 
	{
		return parametro >= 1 && parametro <= 5;
	}

	public int getInteresDeporte() 
	{
		return this.interesDeporte;
	}

	public int getInteresMusica() 
	{
		return this.interesMusica;
	}

	public int getInteresEspectaculo()
	{
		return this.interesEspectaculo;
	}

	public int getInteresCiencia() 
	{
		return this.interesCiencia;
	}
	
	public String getNombre() 
	{
		return this.nombre;
	}
	
	public String toString() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Nombre:"+ this.nombre + "\n");
		sb.append("Interes Deporte:" + this.interesDeporte + "\n");         
		sb.append("Interes Musica:" + this.interesMusica + "\n");
		sb.append("Interes Espectaculo:" + this.interesEspectaculo + "\n");         
		sb.append("Interes Ciencia:" + this.interesCiencia + "\n");
		return sb.toString();
	}	
}