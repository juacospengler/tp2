package grafos;

import java.util.ArrayList;
import java.util.Collections;

import persona.Persona;

public class Kruskal 
{
    private static ArrayList<Arista> aristasKruskal;

    public static ArrayList<Arista> resolver(ArrayList<Persona> vertices, ArrayList<Arista> aristas) 
    {
    	validarVertices(vertices);
		validarAristas(aristas);
        Collections.sort(aristas);
        UnionFind unionFind = new UnionFind(vertices);
        aristasKruskal = new ArrayList<Arista>();
        
        for (Arista arista: aristas) 
        {
            Persona personaOrigen = arista.getVerticeOrigen();
            Persona personaDestino = arista.getVerticeDestino();

            if (aristasKruskal.size() < vertices.size() - 1) 
            {
            	if (!formaCiclo(personaOrigen, personaDestino, unionFind))
            	{
            		aristasKruskal.add(arista);
                    unionFind.unirComponentesConexas(personaOrigen, personaDestino);
            	}
            }
        }
        return aristasKruskal;
    }

    static boolean formaCiclo(Persona verticeOrigen, Persona verticeDestino, UnionFind unionFind) 
    {    	
        return unionFind.formaCiclo(verticeOrigen, verticeDestino);
    }
    
	static boolean validarVertices(ArrayList<Persona> vertices)
	{
		return Grafo.validarVertices(vertices);
	}
	
	static boolean validarAristas(ArrayList<Arista> aristas)
	{
		return Grafo.validarAristas(aristas);
	}
}