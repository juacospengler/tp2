package grafos;

import java.util.ArrayList;

import persona.Persona;

public class Grafo 
{
	public ArrayList<Persona> personas;
	public ArrayList<Arista> aristasAGM;
	private GrafoCompleto grafoCompleto;
	private GrafoAGM grafoAGM;
	private int cantidadComponentesConexas;

	public Grafo(ArrayList<Persona> vertices, int cantidadComponentesConexas) 
	{
		validarVertices(vertices);
		validarCantidadComponentesConexas(cantidadComponentesConexas);
		this.cantidadComponentesConexas = cantidadComponentesConexas;
		this.personas = vertices;
		this.grafoCompleto = crearGrafoCompleto(vertices);
		this.grafoAGM = generarAGM(vertices, grafoCompleto.getAristasCompletas());
		this.aristasAGM = grafoAGM.getAristasAGM();
	}

	public static boolean validarVertices(ArrayList<Persona> vertices) 
	{
		boolean ret = true;
		for (Persona vertice : vertices) 
		{
			ret = ret && vertice != null;
		}

		boolean tamanio = vertices.size() == 0;
		if (!ret || tamanio) 
		{
			throw new IllegalArgumentException("Al menos un vertice o una arista es null");
		} 
		else 
		{
			return ret;
		}
	}

	private void validarCantidadComponentesConexas(int cantidadComponentesConexas)
	{
		if (cantidadComponentesConexas < 2) 
		{
			throw new IllegalArgumentException("La cantidad de componentes conexas debe ser un número entero mayor o igual a 1.");
		}
	}

	private GrafoCompleto crearGrafoCompleto(ArrayList<Persona> vertices) 
	{
		GrafoCompleto grafoCompleto = new GrafoCompleto(vertices);
		return grafoCompleto;
	}

	private GrafoAGM generarAGM(ArrayList<Persona> vertices, ArrayList<Arista> aristas) 
	{
		GrafoAGM grafoAGM = new GrafoAGM(vertices, aristas);
		return grafoAGM;
	}

	public ArrayList<ArrayList<Persona>> darGrupos() 
	{
		ArrayList<ArrayList<Persona>> gruposGrafos = new ArrayList<ArrayList<Persona>>();
		ArrayList<Persona> marcados = new ArrayList<>();
		quitarAristaMayorPeso(this.cantidadComponentesConexas);
		for (Persona vertice : this.personas) 
		{
			if (!marcados.contains(vertice)) 
			{
				marcados.add(vertice);
				ArrayList<Persona> grupo = darComponenteConexa(vertice);
				int acumuladorDeInteresDeporte = 0;
				int acumuladorDeInteresEspectaculo = 0;
				int acumuladorDeInteresCiencia = 0;
				int acumuladorDeInteresMusica = 0;
				for (int elem = 0; elem < grupo.size(); elem++) 
				{
					acumuladorDeInteresDeporte += grupo.get(elem).getInteresDeporte();
					acumuladorDeInteresEspectaculo += grupo.get(elem).getInteresEspectaculo();
					acumuladorDeInteresMusica += grupo.get(elem).getInteresMusica();
					acumuladorDeInteresCiencia += grupo.get(elem).getInteresCiencia();
					marcados.add(grupo.get(elem));
				}
				Persona promedioGrupo = new Persona("Promedio",
					    Math.round(acumuladorDeInteresDeporte / grupo.size()),
					    Math.round(acumuladorDeInteresMusica / grupo.size()),
					    Math.round(acumuladorDeInteresEspectaculo / grupo.size()),
					    Math.round(acumuladorDeInteresCiencia / grupo.size())
					);

				grupo.add(promedioGrupo);
				gruposGrafos.add(grupo);	
			}
		}
		return gruposGrafos;
	}

	private ArrayList<Persona> darComponenteConexa(Persona vertice) 
	{
		UnionFind uf = new UnionFind(this.personas, this.aristasAGM);
		return uf.obtenerVerticesEnMismaComponenteConexa(vertice);
	}

	private void quitarAristaMayorPeso(int cantidadComponentesConexas) 
	{
		for (int i = 1; i < cantidadComponentesConexas; i++) 
		{
			this.aristasAGM.remove(this.aristasAGM.size() - 1);
		}
	}

	public ArrayList<Persona> getPersonas() 
	{
		return this.personas;
	}

	public static boolean validarAristas(ArrayList<Arista> aristas) 
	{
		boolean ret = true;
		for (Arista arista : aristas) 
		{
			ret = ret && arista != null;
		}

		boolean tamanio = aristas.size() == 0;
		if (!ret || tamanio) 
		{
			throw new IllegalArgumentException("Al menos un vertice o una arista es null");
		} 
		else 
		{
			return ret;
		}
	}

	public ArrayList<Arista> getAristasAGM() 
	{
		return aristasAGM;
	}

	public GrafoCompleto getGrafoCompleto() 
	{
		return grafoCompleto;
	}

	public GrafoAGM getGrafoAGM() 
	{
		return grafoAGM;
	}
}