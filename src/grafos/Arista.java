package grafos;

import persona.Persona;

public class Arista implements Comparable<Arista> {
	private Persona verticeOrigen;
	private Persona verticeDestino;
	private int similaridad;
	
	public Arista(Persona personaOrigen, Persona personaDestino) 
	{
		validarparametros(personaOrigen,  personaDestino);
		this.verticeOrigen = personaOrigen;
		this.verticeDestino = personaDestino;
		this.similaridad = calcularSimilaridad();
	}

	int calcularSimilaridad() 
	{
		return  Math.abs(this.verticeOrigen.getInteresCiencia()     - 	this.verticeDestino.getInteresCiencia()) +
				Math.abs(this.verticeOrigen.getInteresMusica()      - 	this.verticeDestino.getInteresMusica())  +
				Math.abs(this.verticeOrigen.getInteresDeporte()     - 	this.verticeDestino.getInteresDeporte()) +
				Math.abs(this.verticeOrigen.getInteresEspectaculo() - 	this.verticeDestino.getInteresEspectaculo());	
	}


	public Persona getVerticeOrigen() 
	{
		return verticeOrigen;
	}

	public Persona getVerticeDestino() 
	{
		return verticeDestino;
	}

	public int getSimilaridad() 
	{
		return similaridad;
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Arista \n");
		sb.append("Vertice Origen " + this.verticeOrigen.toString() + "\n");         
		sb.append("Vertice Destino " + this.verticeDestino.toString() + "\n");
		sb.append("Peso " + this.similaridad);
		return sb.toString();
	}
	
	private void validarparametros(Persona personaOrigen, Persona personaDestino) 
	{	
		if (personaOrigen == null || personaDestino == null) 
		{
			throw new IllegalArgumentException ("Alguna persona es nula");
		}
	}
	
	public boolean iguales(Arista obj) 
	{
	    return  this.verticeOrigen.iguales(obj.verticeOrigen) &&
	    		this.verticeDestino.iguales(obj.verticeDestino) &&
	    		this.similaridad == obj.getSimilaridad();
	}
	
	@Override
	public int compareTo(Arista o) 
	{
		return Integer.compare(similaridad, o.getSimilaridad());
	}
}
