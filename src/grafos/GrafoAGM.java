package grafos;

import java.util.ArrayList;

import persona.Persona;

public class GrafoAGM 
{
	private ArrayList<Persona> verticesAGM;
	private ArrayList<Arista> aristasAGM;

	public GrafoAGM(ArrayList<Persona> vertices, ArrayList<Arista> aristas) 
	{
		validarVertices(vertices);
		validarAristas(aristas);
		this.verticesAGM = vertices;
		this.aristasAGM = crearAristasAGM(vertices, aristas);
	}

	private ArrayList<Arista> crearAristasAGM(ArrayList<Persona> vertices, ArrayList<Arista> aristas) 
	{
		return Kruskal.resolver(vertices, aristas);
	}

	private void validarVertices(ArrayList<Persona> vertices) 
	{
		Grafo.validarVertices(vertices);
	}

	private void validarAristas(ArrayList<Arista> aristas) 
	{
		Grafo.validarAristas(aristas);
	}

	public ArrayList<Persona> getVerticesAGM() 
	{
		return this.verticesAGM;
	}

	public ArrayList<Arista> getAristasAGM() 
	{
		return this.aristasAGM;
	}
}