package grafos;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Test;

import persona.Persona;

public class GrafoAGMTest 
{
	@Test(expected = IllegalArgumentException.class)
	public void grafoAGMSinVertices() 
	{
		ArrayList<Persona> personas = new ArrayList<Persona>();
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Arista arista1 = new Arista(per1, per2);
		ArrayList<Arista> aristas = new ArrayList<Arista>();
		aristas.add(arista1);
		
		GrafoAGM grafoAGM = new GrafoAGM(personas, aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafoAGMConVerticeNulo() 
	{
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per3 = null;
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
	
		Arista arista1 = new Arista(per1, per2);
		ArrayList<Arista> aristas = new ArrayList<Arista>();
		aristas.add(arista1);
		
		GrafoAGM grafoAGM = new GrafoAGM(personas, aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafoAGMSinAristas() 
	{
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per3 = new Persona("ana", 2, 1, 5, 1);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
	
		ArrayList<Arista> aristas = new ArrayList<Arista>();	
		
		GrafoAGM grafoAGM = new GrafoAGM(personas, aristas);
	}	
	
	@Test
	public void grafoAGMGeneradoOk() 
	{ 
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per3 = new Persona("ana", 2, 1, 5, 1);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		
		ArrayList<Arista> aristas = new ArrayList<Arista>();	
		aristas.add(new Arista(per1, per2));
		aristas.add(new Arista(per1, per3));
		aristas.add(new Arista(per2, per3));
		
		GrafoAGM grafoAGM = new GrafoAGM(personas, aristas);
		
		assertNotNull(grafoAGM);
	}
}