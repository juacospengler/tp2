package grafos;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Test;

import persona.Persona;


public class GrafoTest
{	
	@Test(expected = IllegalArgumentException.class)
	public void grafoSinPersonas()
	{	
		ArrayList<Persona> personas = new ArrayList<Persona>();
		int cantidadComponentesConexas = 2;
		Grafo grafo = new Grafo(personas, cantidadComponentesConexas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafosSinComponentesConexas()
	{	
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona("julio",2,3,4,5));
		personas.add(new Persona("ma",1,3,4,2));
		personas.add(new Persona("re",2,3,1,5));
		personas.add(new Persona("po",5,4,4,4));
		int cantidadComponentesConexas = 1;
		Grafo grafo = new Grafo(personas, cantidadComponentesConexas);
	}
	
	@Test
	public void grafoBienCreado() 
	{ 
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona("julio",2,3,4,5));
		personas.add(new Persona("ma",1,3,4,2));
		personas.add(new Persona("re",2,3,1,5));
		personas.add(new Persona("po",5,4,4,4));
		int cantidadComponentesConexas = 2;
		
		Grafo grafo = new Grafo(personas, cantidadComponentesConexas);
		
		assertNotNull(grafo);
	}
}