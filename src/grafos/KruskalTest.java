package grafos;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import persona.Persona;

public class KruskalTest 
{
	
	@Test(expected = IllegalArgumentException.class)
	public void kruskalSinVertices() 
	{
		ArrayList<Persona> personas = new ArrayList<Persona>();
	
		Kruskal.validarVertices(personas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void kruskalConVerticeNulo() 
	{
		Persona per1 = null;
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
	
		Kruskal.validarVertices(personas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void kruskalSinAristas() 
	{
		ArrayList<Arista> aristas = new ArrayList<Arista>();
	
		Kruskal.validarAristas(aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void kruskalConAristaNula() 
	{
		Persona per1 = null;
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per3 = new Persona("amincito", 4, 3, 2, 1);
		Arista arista1 = new Arista(per1, per2);
		Arista arista2 = new Arista(per3, per2);
		ArrayList<Arista> aristas = new ArrayList<Arista>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Kruskal.validarAristas(aristas);
	}
	
	@Test
	public void kruskalConVerticesNoVacios() 
	{ 
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona("joaquin", 1, 2, 3, 4));
		
		assertEquals(true,Kruskal.validarVertices(personas));
	}
	
	@Test
	public void kruskalConAristasNoVacias() 
	{ 
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per3 = new Persona("amincito", 4, 3, 2, 1);
		ArrayList<Arista> aristas = new ArrayList<Arista>();
		aristas.add(new Arista(per2, per3));
		
		assertEquals(true, Kruskal.validarAristas(aristas));
	}	
	
	@Test
	public void kruskalSiFormanCiclo() 
	{
		Persona per1 = new Persona("ana", 1, 3, 5, 4);
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per3 = new Persona("amincito", 4, 3, 2, 1);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		
		UnionFind unionFind = new UnionFind(personas);
		unionFind.unirComponentesConexas(per1, per2);
		unionFind.unirComponentesConexas(per1, per3);
		
		assertEquals(true, Kruskal.formaCiclo(per2, per3, unionFind));
	}
	
	@Test
	public void kruskalNoFormanCiclo() 
	{
		Persona per1 = new Persona("ana", 1, 3, 5, 4);
		Persona per2 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per3 = new Persona("amincito", 4, 3, 2, 1);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		
		UnionFind unionFind = new UnionFind(personas);
		unionFind.unirComponentesConexas(per1, per2);
		
		assertEquals(false, Kruskal.formaCiclo(per2, per3, unionFind));
	}
	
	@Test
	public void kruskalSiResuelve() 
	{
		Persona per0 = new Persona("0", 1, 1, 1, 1);
		Persona per1 = new Persona("1", 2, 1, 1, 2);
		Persona per2 = new Persona("2", 1, 1, 1, 2);
		Persona per3 = new Persona("3", 2, 1, 1, 5);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per0);
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		
		ArrayList<Arista> aristasSolucion = new ArrayList<Arista>();
		aristasSolucion.add(new Arista(per0, per2));
		aristasSolucion.add(new Arista(per1, per2));
		aristasSolucion.add(new Arista(per1, per3));
		
		Grafo grafo = new Grafo(personas, 2);
		ArrayList<Arista> aristasAGM = grafo.getAristasAGM();
		
		boolean ret = true;
		for (int i = 0; i < aristasAGM.size(); i++)
		{
			ret = ret && aristasAGM.get(i).iguales(aristasSolucion.get(i));
		}
		
		assertEquals(true, ret);
	}
	
	@Test
	public void kruskalNoResuelve() 
	{
		Persona per0 = new Persona("0", 1, 1, 1, 1);
		Persona per1 = new Persona("1", 2, 1, 1, 2);
		Persona per2 = new Persona("2", 1, 1, 1, 2);
		Persona per3 = new Persona("3", 2, 1, 1, 5);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per0);
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		
		ArrayList<Arista> aristasSolucion = new ArrayList<Arista>();
		aristasSolucion.add(new Arista(per0, per2));
		aristasSolucion.add(new Arista(per1, per2));
		aristasSolucion.add(new Arista(per0, per1));
		
		Grafo grafo = new Grafo(personas, 2);
		ArrayList<Arista> aristasAGM = grafo.getAristasAGM();
		
		boolean ret = true;
		for (int i = 0; i < aristasAGM.size(); i++)
		{
			ret = ret && aristasAGM.get(i).iguales(aristasSolucion.get(i));
		}
		
		assertEquals(false, ret);
	}
}