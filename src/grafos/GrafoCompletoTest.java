package grafos;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import persona.Persona;

public class GrafoCompletoTest 
{
	
	@Test
	public void GrafoCompletoBien() {
		Persona per1 = new Persona("julio", 2, 3, 4, 5);
		Persona per2 = new Persona("ma", 1, 3, 4, 2);
		Persona per3 = new Persona("re", 2, 3, 1, 5);
		Persona per4 = new Persona("po", 5, 4, 4, 4);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		personas.add(per4);

		GrafoCompleto grafo = new GrafoCompleto(personas);
		assertEquals(6, grafo.getAristasCompletas().size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void grafoSinVertices() {
		Persona invalido = null;
		ArrayList<Persona> unaPersona = new ArrayList<Persona>();
		unaPersona.add(invalido);
		GrafoCompleto grafo = new GrafoCompleto(unaPersona);
	}

	@Test
	public void grafoConUnVertice() {
		Persona per1 = new Persona("julio", 2, 3, 4, 5);
		ArrayList<Persona> unaPersona = new ArrayList<Persona>();
		unaPersona.add(per1);

		GrafoCompleto grafo = new GrafoCompleto(unaPersona);

		assertEquals(0, grafo.getAristasCompletas().size());
	}
}