package grafos;

import java.util.ArrayList;

import persona.Persona;

public class GrafoCompleto 
{
	public ArrayList<Persona> vertices;
	private ArrayList<Arista> aristasCompletas;

	public GrafoCompleto(ArrayList<Persona> vertices) 
	{
		verticesValidos(vertices);
		this.vertices = vertices;
		this.aristasCompletas = crearAristas();
	}

	private ArrayList<Arista> crearAristas() 
	{
		ArrayList<Arista> aristasGrafoCompleto = new ArrayList<>();
		for (Persona personaOrigen : this.vertices) 
		{
			for (Persona personaDestino : this.vertices) 
			{
				if (this.vertices.indexOf(personaOrigen) < this.vertices.indexOf(personaDestino)) 
				{
					agregarArista(aristasGrafoCompleto, new Arista(personaOrigen, personaDestino));
				}
			}
		}
		return aristasGrafoCompleto;
	}

	void agregarArista(ArrayList<Arista> aristasGrafo, Arista arista) 
	{
		aristasGrafo.add(arista);
	}

	public ArrayList<Arista> getAristasCompletas() 
	{
		return this.aristasCompletas;
	}
	
	private void verticesValidos(ArrayList<Persona> vertices) 
	{
		Grafo.validarVertices(vertices);
	}
}