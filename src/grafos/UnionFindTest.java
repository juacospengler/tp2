package grafos;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import persona.Persona;

public class UnionFindTest 
{
	@Test(expected = IllegalArgumentException.class)
	public void unionFindConVerticeNulo(){
		Persona per1 = new Persona("julio",2,3,4,5);
		Persona per2 = null;
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		UnionFind uf = new UnionFind(personas);	
	}
	
	@Test
	public void buscarRaizMismoPadre()
	{
		Persona per1 = new Persona("julio",2,3,4,5);
		Persona per2 = new Persona("ma",1,3,4,2);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		
		UnionFind uf = new UnionFind(personas);	
		uf.unirComponentesConexas(per1, per2);
		Persona aux = uf.buscarRaiz(per2);
		
		assertTrue(per1.iguales(aux));
	}	

	public void buscarRaizDistintoPadre()
	{
		Persona per1 = new Persona("julio",2,3,4,5);
		Persona per2 = new Persona("ma",1,3,4,2);
		Persona per3 = new Persona("re",2,3,1,5);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		
		UnionFind uf = new UnionFind(personas);	
		uf.unirComponentesConexas(per1, per2);
		Persona aux = uf.buscarRaiz(per2);
		
		assertFalse(per1.iguales(aux));
	}	

	@Test
	public void distintoComponenteConexoTest() 
	{
		Persona per1 = new Persona("julio",2,3,4,5);
		Persona per2 = new Persona("ma",1,3,4,2);
		Persona per3 = new Persona("re",2,3,1,5);
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(per1);
		personas.add(per2);
		personas.add(per3);
		
		UnionFind uf = new UnionFind(personas);	
		uf.unirComponentesConexas(per1, per2);
		
		assertFalse(uf.mismaComponenteConexa(per1,per3));
	}
	
	@Test
	public void mismoComponenteConexoTest() 
	{
	    Persona per1 = new Persona("julio", 2, 3, 4, 5); 
	    Persona per2 = new Persona("ma", 1, 3, 4, 2);
	    ArrayList<Persona> personas = new ArrayList<Persona>();
	    personas.add(per1);
	    personas.add(per2);
	    
	    UnionFind uf = new UnionFind(personas);	   
	    uf.unirComponentesConexas(per1, per2);
	    
	    assertTrue(uf.mismaComponenteConexa(per1, per2)); 
	}
}