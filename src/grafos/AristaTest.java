package grafos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import persona.Persona;
public class AristaTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void aristaSinVerticeOrigen() 
	{
		Persona per0 = new Persona("joaquin", 1, 2, 3, 4);
		Arista arista = new Arista(null, per0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void aristaSinVerticeDestino() 
	{
		Persona per0 = new Persona("joaquin", 1, 2, 3, 4);
		Arista arista = new Arista(per0, null);
	}
	
	@Test
	public void aristaBienCreada() 
	{ 
		Persona per0 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		
		Arista arista = new Arista(per0, per1);
		
		assertNotNull(arista);
	}
	
	@Test
	public void aristaCalcularSimilaridadOk() 
	{ 
		Persona per0 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		Arista arista = new Arista(per0, per1);
		
		int similaridad = arista.calcularSimilaridad();
		
		assertEquals(8, similaridad);
	}
	
	@Test
	public void aristaCalcularSimilaridadNoOk() 
	{ 
		Persona per0 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		Arista arista = new Arista(per0, per1);
		
		int similaridad = arista.calcularSimilaridad();
		
		assertFalse(similaridad == 10);
	}
	
	@Test
	public void aristaSonIguales() 
	{ 
		Persona per0 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per1 = new Persona("amincito", 4, 3, 2, 1);
		Arista arista1 = new Arista(per0, per1);
		Arista arista2 = new Arista(per0, per1);
		
		assertTrue(arista1.iguales(arista2));
	}
	
	@Test
	public void aristaSonDistintas() 
	{ 
		Persona per0 = new Persona("joaquin", 1, 2, 3, 4);
		Persona per1 = new Persona("amincito", 4, 3, 2, 2);
		Persona per2 = new Persona("amincitoxd", 4, 4, 2, 2);
		Arista arista1 = new Arista(per0, per1);
		Arista arista2 = new Arista(per0, per2);
		
		assertFalse(arista1.iguales(arista2));
	}
}