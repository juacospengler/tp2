package grafos;

import java.util.ArrayList;
import java.util.HashMap;

import persona.Persona;

public class UnionFind 
{
	private HashMap<Persona, Persona> raicesPersonas;

	public UnionFind(ArrayList<Persona> vertices) 
	{
		validarVertices(vertices);
		this.raicesPersonas = new HashMap<>();
		for (Persona persona : vertices) 
		{
			this.raicesPersonas.put(persona, persona);
		}
	}

	public UnionFind(ArrayList<Persona> vertices, ArrayList<Arista> aristas) 
	{
		validarVertices(vertices);
		this.raicesPersonas = new HashMap<>();
		for (Persona persona : vertices) 
		{
			this.raicesPersonas.put(persona, persona);
		}
		
		for (Arista arista : aristas)
		{
			Persona personaOrigen = arista.getVerticeOrigen();
			Persona personaDestino = arista.getVerticeDestino();
			unirComponentesConexas(personaOrigen, personaDestino);
		}
	}

	public boolean formaCiclo(Persona verticeOrigen, Persona verticeDestino) 
	{
		return mismaComponenteConexa(verticeOrigen, verticeDestino);
	}

	public boolean mismaComponenteConexa(Persona verticeOrigen, Persona verticeDestino) 
	{
		Persona raizVOrigen = buscarRaiz(verticeOrigen);
		Persona raizVDestino = buscarRaiz(verticeDestino);
		return raizVOrigen.iguales(raizVDestino);
	}

	public Persona buscarRaiz(Persona persona) 
	{
		Persona raizDeVertice = this.raicesPersonas.get(persona);
		while (!persona.iguales(raizDeVertice)) 
		{
			persona = raizDeVertice;
			raizDeVertice = this.raicesPersonas.get(persona);
		}

		return raizDeVertice;
	}

	public void unirComponentesConexas(Persona verticeOrigen, Persona verticeDestino) 
	{
		if (verticeDestino.iguales(this.raicesPersonas.get(verticeDestino)))
		{
			this.raicesPersonas.put(verticeDestino, verticeOrigen);
		}
		else
		{
			this.raicesPersonas.put(verticeOrigen, verticeDestino);
		}
	}

	private static void validarVertices(ArrayList<Persona> vertices) 
	{
		Grafo.validarVertices(vertices);
	}

	public ArrayList<Persona> obtenerVerticesEnMismaComponenteConexa(Persona verticeOrigen) 
	{
		ArrayList<Persona> verticesEnMismaComponente = new ArrayList<>();
		Persona raizOrigen = buscarRaiz(verticeOrigen);

		for (Persona vertice : raicesPersonas.keySet()) 
		{
			if (buscarRaiz(vertice).iguales(raizOrigen)) 
			{
				verticesEnMismaComponente.add(vertice);
			}
		}

		return verticesEnMismaComponente;
	}
}