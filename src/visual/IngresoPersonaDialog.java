package visual;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

//import juego.PantallaJuego;
import persona.Persona;


//Clase interna para la ventana de ingreso de persona
public class IngresoPersonaDialog extends JDialog 
{
	private static Persona persona;
	private JTextField nombreField;
	private JComboBox<String> interesDeporteBox;
	private JComboBox<String> interesMusicaBox;
	private JComboBox<String> interesEspectaculoBox;
	private JComboBox<String> interesCienciaBox;

	public IngresoPersonaDialog(Frame parent) 
	{
		super(parent, "Ingresar Persona", true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(484, 343);
		setLocationRelativeTo(parent);
		this.persona = null;

		JPanel panel = new JPanel();
		nombreField = new JTextField();
		nombreField.setBounds(201, 29, 228, 28);
		panel.setLayout(null);

		this.interesDeporteBox = new JComboBox();
		this.interesDeporteBox.setBounds(222, 83, 42, 22);
		this.interesDeporteBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5" }));
		panel.add(this.interesDeporteBox);

		this.interesMusicaBox = new JComboBox();
		this.interesMusicaBox.setBounds(222, 116, 42, 22);
		this.interesMusicaBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5" }));
		panel.add(this.interesMusicaBox);

		this.interesEspectaculoBox = new JComboBox();
		this.interesEspectaculoBox.setBounds(222, 149, 42, 22);
		this.interesEspectaculoBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5" }));
		panel.add(this.interesEspectaculoBox);

		this.interesCienciaBox = new JComboBox();
		this.interesCienciaBox.setBounds(222, 182, 42, 22);
		this.interesCienciaBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5" }));
		panel.add(this.interesCienciaBox);
		
		addWindowListener(new WindowAdapter() 
		{
		    @Override
		    public void windowClosing(WindowEvent e) 
		    {
		    	dispose();
		    }
		});
		crearLabels(panel);
		
		crearBotonGuardar(panel);
		getContentPane().add(panel);
	}

	private void crearBotonGuardar(JPanel panel) 
	{
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(107, 251, 234, 28);
		btnGuardar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String nombre = nombreField.getText();
				
				if (nombre.isEmpty()) 
				{
					mostrarMensaje("El nombre no puede estar vacío.");
				} 
				else 
				{
					int interesDeporte = Integer
							.parseInt((String) interesDeporteBox.getItemAt(interesDeporteBox.getSelectedIndex()));
					int interesMusica = Integer
							.parseInt((String) interesMusicaBox.getItemAt(interesMusicaBox.getSelectedIndex()));
					int interesEspectaculo = Integer.parseInt(
							(String) interesEspectaculoBox.getItemAt(interesEspectaculoBox.getSelectedIndex()));
					int interesCiencia = Integer
							.parseInt((String) interesCienciaBox.getItemAt(interesCienciaBox.getSelectedIndex()));
					
					persona = new Persona(nombre, interesDeporte, interesMusica, interesEspectaculo, interesCiencia);
					dispose();
				}
			}
		});
		panel.add(btnGuardar);
	}

	private void crearLabels(JPanel panel) 
	{
		JLabel label = new JLabel("Nombre:");
		label.setBounds(37, 17, 76, 52);
		panel.add(label);
		panel.add(nombreField);

		JLabel lblInteresDeporte = new JLabel("Interes Deporte:");
		lblInteresDeporte.setBounds(37, 80, 114, 28);
		panel.add(lblInteresDeporte);

		JLabel lblInteresMusica = new JLabel("Interes Musica");
		lblInteresMusica.setBounds(37, 113, 131, 28);
		panel.add(lblInteresMusica);

		JLabel lblInteresEspectaculo = new JLabel("Interes Espectaculo:");
		lblInteresEspectaculo.setBounds(37, 146, 131, 28);
		panel.add(lblInteresEspectaculo);

		JLabel lblInteresCiencia = new JLabel("Interes Ciencia:");
		lblInteresCiencia.setBounds(37, 179, 97, 28);
		panel.add(lblInteresCiencia);
	}

	public Persona getPersonaNueva() 
	{
		return persona;
	}

	private void mostrarMensaje(String mensaje) 
	{
		JOptionPane.showMessageDialog(this, mensaje);
	}
}