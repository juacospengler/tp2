package visual;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import grafos.Grafo;
import persona.Persona;

public class PantallaPrincipal 
{
	private JFrame frame;
	private ArrayList<Persona> personasingresadas;
	private JButton btnIngresarPersona;
	private JButton btnmostrarRelacion;
	private JTable table;
	private DefaultTableModel tableModel;
	private JComboBox intCantidadGrupos;

	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					PantallaPrincipal window = new PantallaPrincipal();
					window.frame.setVisible(true);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	public PantallaPrincipal() 
	{
		initialize();
	}

	private void initialize() 
	{
		personasingresadas = new ArrayList<>();
		frame = new JFrame("Clustering Humano");
		frame.setBounds(100, 100, 797, 489);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		crearBotonIncripcionPersona(frame);
		crearMostrarRelacion(frame);

		tableModel = new DefaultTableModel();
		tableModel.addColumn("Nombre");
		tableModel.addColumn("Interes Deporte");
		tableModel.addColumn("Interes Musica");
		tableModel.addColumn("Interes Espectaculo");
		tableModel.addColumn("Interes Ciencia");
		tableModel.addColumn("Grupo");

		table = new JTable(tableModel);
		table.setEnabled(false);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 10, 761, 396);
		frame.getContentPane().add(scrollPane);
		frame.getContentPane().setLayout(null);

		intCantidadGrupos = new JComboBox<Integer>();
		intCantidadGrupos.setModel(new DefaultComboBoxModel<>(new Integer[] { 2, 3, 4, 5 }));
		intCantidadGrupos.setSelectedIndex(0); // Establece el valor predeterminado en 1
		intCantidadGrupos.setBounds(471, 417, 40, 20);
		frame.getContentPane().add(intCantidadGrupos);

		JLabel lblNewLabel = new JLabel("Cantidad de Grupos:");
		lblNewLabel.setBounds(351, 420, 114, 14);
		frame.getContentPane().add(lblNewLabel);

	}

	private void crearBotonIncripcionPersona(JFrame frame) 
	{
		JButton btnIngresarPersona = new JButton("Ingresar Persona");
		btnIngresarPersona.setBounds(10, 416, 240, 23);
		frame.getContentPane().add(btnIngresarPersona);
		btnIngresarPersona.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				IngresoPersonaDialog dialog = new IngresoPersonaDialog(frame);
				dialog.setVisible(true);
				Persona personaIngresada = dialog.getPersonaNueva();
				if (personaIngresada != null) 
				{
					personasingresadas.add(personaIngresada);
					mostrarPersonas();
				}
			}
		});
	}

	private void crearMostrarRelacion(JFrame frame) 
	{
		JButton btnmostrarRelacion = new JButton("Mostrar Grupos");
		btnmostrarRelacion.setBounds(553, 416, 218, 23);
		frame.getContentPane().add(btnmostrarRelacion);
		btnmostrarRelacion.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				mostrarRelacionDePersonas();
			}
		});
	}

	private void mostrarPersonas() 
	{
		tableModel.setRowCount(0);
		for (Persona persona : personasingresadas) 
		{
			Object[] rowData = { persona.getNombre(), persona.getInteresDeporte(), persona.getInteresEspectaculo(),
					persona.getInteresMusica(), persona.getInteresCiencia(), 0 };
			tableModel.addRow(rowData);
		}
	}

	private void mostrarRelacionDePersonas() 
	{
		int numeroDeGrupos = (Integer) intCantidadGrupos.getSelectedItem();
		if (validarNumeroGrupos(numeroDeGrupos)) 
		{
			tableModel.setRowCount(0);
			Grafo grafo = new Grafo(personasingresadas, numeroDeGrupos);
			ArrayList<ArrayList<Persona>> gruposGrafos = grafo.darGrupos();
			for (int nroGrupo = 0; nroGrupo < gruposGrafos.size(); nroGrupo++) 
			{
				ArrayList<Persona> elementosDelGrupo = gruposGrafos.get(nroGrupo);
				for (Persona persona : elementosDelGrupo) 
				{
					Object[] personasDatos = { persona.getNombre(), persona.getInteresDeporte(),
							persona.getInteresEspectaculo(), persona.getInteresMusica(), persona.getInteresCiencia(),
							(nroGrupo + 1) };
					tableModel.addRow(personasDatos);
				}
			}

		}
		else 
		{
			mostrarMensaje("La cantidad de grupos debe ser menor o igual a la cantidad de personas ingresadas");
			mostrarPersonas();
		}
	}

	private boolean validarNumeroGrupos(int numero) 
	{
		return (numero <= this.personasingresadas.size());
	}

	private void mostrarMensaje(String mensaje) 
	{
		JOptionPane.showMessageDialog(frame, mensaje);
	}
}